# FIFA Project

## How to run 🏃🏻

On iOS: 
```bash
yarn ios
```

On Android:
```bash
yarn android
```

---
## Introduction
This project was made using:
```bash
npx react-native init <ProjectName>
```

Other versions of software used:

| Node | 14.17.3 |
| ---: | -------: |

| Yarn | 1.22.10 |
| ---: | -------: |

| npm | 7.19.1 |
| ---: | -------: |

### Packages used in the project:
| Library        | Version           |
| ------------- |:-------------:|
| [react](https://www.npmjs.com/package/react) | 17.0.1 |
| [react-native](https://www.npmjs.com/package/react-native) | 0.64.2 |
| [react-native-gesture-handler](https://www.npmjs.com/package/react-native-gesture-handler) | ^1.10.3 |
| [react-native-safe-area-context](https://www.npmjs.com/package/react-native-safe-area-context) | ^3.2.0 |
| [react-navigation](https://www.npmjs.com/package/react-navigation) | ^4.4.4 |
| [react-navigation-stack](https://www.npmjs.com/package/react-navigation-stack) | ^2.10.4 |
| [react-redux](https://www.npmjs.com/package/react-redux) | ^7.2.4 |
| [redux](https://www.npmjs.com/package/redux) | ^4.1.0 |
| [redux-saga](https://www.npmjs.com/package/redux-saga) | ^1.1.3 |

## Source Control and Branching Strategy

### The main branches will be the following:

```
development
``` 

```
testing
``` 

```
staging
``` 

```
production
``` 

### For particular features and bugs 🐛:

```
    features/[JIRA_ID]-[FEATURE_NAME]-[FEATURE_DESCRIPTION]
```
```
    bugs/[JIRA_ID]-[BUG_NAME]-[BUG_DESCRIPTION]
```




