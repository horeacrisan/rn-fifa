import React, { useEffect } from 'react';
import { View, Text, Button } from 'react-native';
import { connect } from 'react-redux';
import { GET_ALL_USER_INFO_REQUEST } from '../../modules/user/actions';
import { styles } from './style';


const mapStateToProps = (state, _props) => {
  const { id, name, email } = state.user;

  return { id, name, email };
};

const mapDispatchToProps = (dispatch, _props) => ({
  getAllUserInfo: () => {
    dispatch({
      type: GET_ALL_USER_INFO_REQUEST,
      payload: {},
    });
  },
});

//Basically, what is seen on the screen
const HomeView = ({ id, name, email, getAllUserInfo, navigation }) => {
  useEffect(() => {
    getAllUserInfo();
  }, [getAllUserInfo]);

  return (
    <View>
      <Button
        title="Take me to survey screen"
        onPress={() => { console.log(navigation.navigate("Survey")) }}>
      </Button>
      <View>
        <Text style={styles.text}>{id}</Text>
        <Text style={styles.text}>Hello, {name}</Text>
        <Text style={styles.text}>{email}</Text>
      </View>
    </View>
  );
};

const HomeScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeView);

export { HomeScreen };
