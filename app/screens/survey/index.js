import React from 'react';
import { View, Text } from 'react-native';
import { styles } from './style'

//Basically, what is seen on the screen
const SurveyScreen = () => {
  // navigation.navigate('Login')

  return (
    <View>
      <Text styles={styles.text}>There was a cat named Bob</Text>
    </View>
  );
};

export { SurveyScreen };
