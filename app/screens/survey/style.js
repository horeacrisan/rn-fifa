import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
        justifyContent: "center",
        alignItems: "center",
        padding: 28,
        fontSize: 24,
      }
});

export {styles};