import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import { HomeScreen } from '../screens/home/index';
import { SurveyScreen } from '../screens/survey/index';

const MainStack = createStackNavigator({
    Home: {
        screen: HomeScreen
    },
    Survey: {
        screen: SurveyScreen
    }
});

const Navigation = createAppContainer(MainStack);

export { Navigation };
