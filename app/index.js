import React from 'react';
import { View } from 'react-native';
import { Navigation } from './utils/router';
import { tecnet } from 'tecnet'; // testing a custom NPM package 📦
import { styles } from './style';

const App = ({ }) => {
    tecnet();
    return (<View style={styles.app}>
        <Navigation />
    </View>);
}

export { App };
